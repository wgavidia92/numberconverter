package numberconverter;

import java.lang.System.Logger;
import java.lang.System.Logger.Level;
import java.util.Scanner;

import numberconverter.api.NumberAPI;
import numberconverter.api.StringToBinaryAPI;
import numberconverter.converter.Converter;
import numberconverter.converter.Converter.ConverterType;

public class App {

	private static final Logger LOG = System.getLogger(App.class.getName());

	public static void main(String[] args) {
		try (var in = new Scanner(System.in)) {
			var octalConverter = new NumberAPI(Converter.getInstance(ConverterType.OCTAL));
			var hexadecimalConverter = new NumberAPI(Converter.getInstance(ConverterType.HEXADECIMAL));
			var binaryConverter = new NumberAPI(Converter.getInstance(ConverterType.BINARY));

			LOG.log(Level.INFO, "Type the decimal to convert and then press [ENTER]");
			var number = in.next();

			var octal = octalConverter.convert(number);
			var hexadecimal = hexadecimalConverter.convert(number);
			var binary = binaryConverter.convert(number);

			var message = """
					
					The number in octal notation is %s
					The number in hexadecimal notation is %s
					The number in binary notation is %s
					"""
					.formatted(octal, hexadecimal, binary);

			LOG.log(Level.INFO, message);

			var strToBinary = new StringToBinaryAPI(Converter.getInstance(ConverterType.BINARY));
			LOG.log(Level.INFO, strToBinary.of("Wilmer Gavidia"));
		}
	}

}
