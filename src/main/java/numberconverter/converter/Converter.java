package numberconverter.converter;

public interface Converter {

	enum ConverterType {
		OCTAL,
		HEXADECIMAL,
		BINARY
	}

	String convert(long number);

	static Converter getInstance(ConverterType option) {
		return switch (option) {
			case OCTAL -> new DecimalToOctalConverter();
			case HEXADECIMAL -> new DecimalToHexadecimalConverter();
			case BINARY -> new DecimalToBinaryConverter();
		};
	}

}
