package numberconverter.converter;

import java.util.Map;

final class DecimalToHexadecimalConverter implements Converter, NumberConverter {

	private static final Map<Long, String> HEXADECIMAL_VALUES = Map.of(
				10L, "A",
				11L, "B",
				12L, "C",
				13L, "D",
				14L, "E",
				15L, "F"
			);

	@Override
	public String convert(long number) {
		return convert(Notation.HEXA, number, this::convertLongToHexadecimal);
	}

	private String convertLongToHexadecimal(Long number) {
		return HEXADECIMAL_VALUES.getOrDefault(number, number.toString());
	}

}
