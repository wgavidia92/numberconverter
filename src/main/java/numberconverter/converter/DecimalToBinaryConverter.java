package numberconverter.converter;

final class DecimalToBinaryConverter implements Converter, NumberConverter {

	@Override
	public String convert(long number) {
		return convert(Notation.BINARY, number, Long::toString);
	}

}
