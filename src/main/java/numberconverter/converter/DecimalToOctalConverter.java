package numberconverter.converter;

final class DecimalToOctalConverter implements Converter, NumberConverter {

	@Override
	public String convert(long number) {
		return convert(Notation.OCTAL, number, Long::toString);
	}

}
