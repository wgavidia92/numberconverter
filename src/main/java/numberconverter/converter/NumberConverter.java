package numberconverter.converter;

import java.util.function.LongFunction;

sealed interface NumberConverter
	permits DecimalToBinaryConverter, DecimalToHexadecimalConverter, DecimalToOctalConverter {

	enum Notation {
		BINARY(2),
		OCTAL(8),
		HEXA(16),
		MY_NOT(4)
		;

		private final long limit;

		Notation(long limit) { this.limit = limit; }
	}

	default String convert(Notation notation, long number, LongFunction<String> mapper) {
		var builder = new StringBuilder();

		while (true) {
			if (number < notation.limit) {
				builder.append(mapper.apply(number));
				break;
			}

			builder.append(mapper.apply(number % notation.limit));
			number = Math.floorDiv(number, notation.limit);
		}

		return builder.reverse().toString();
	}

}
