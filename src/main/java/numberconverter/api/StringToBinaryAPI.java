package numberconverter.api;

import java.util.stream.Collectors;
import java.util.stream.Stream;

import numberconverter.converter.Converter;

public class StringToBinaryAPI {

	private final Converter converter;

	public StringToBinaryAPI(Converter converter) {
		this.converter = converter;
	}

	public String of(String value) {
		return Stream.of(value.chars())
			.flatMap(c -> c.boxed())
			.map(converter::convert)
			.collect(Collectors.joining(" "));
	}

}
