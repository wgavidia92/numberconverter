package numberconverter.api;

import java.lang.System.Logger;
import java.lang.System.Logger.Level;

import numberconverter.converter.Converter;

public class NumberAPI {

	private static final Logger LOG = System.getLogger(NumberAPI.class.getName());

	private final Converter converter;

	public NumberAPI(Converter converter) {
		this.converter = converter;
	}

	public String convert(String value) {
		if (!value.matches("\\d+")) {
			LOG.log(Level.ERROR, "Invalid value {0}", value);
			throw new IllegalArgumentException("Invalid value");
		}

		var number = Long.valueOf(value);
		return converter.convert(number);
	}

}
