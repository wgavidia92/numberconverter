package numberconverter.api;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import numberconverter.converter.Converter;

class NumberAPITest {

	private NumberAPI numberAPI;
	private Converter converter;

	@BeforeEach
	void init() {
		converter = Mockito.mock(Converter.class);
		numberAPI = new NumberAPI(converter);
	}

	@Test
	@DisplayName("Should return 1 without exceptions")
	void convertTest() {
		var expected = "1";
		Mockito.when(converter.convert(Mockito.anyLong()))
			.thenReturn("1");

		var result = numberAPI.convert("1");
		Assertions.assertEquals(expected, result);
	}

	@Test
	@DisplayName("Should throw an exception caused by wrong parameter")
	void convertWithExceptionTest() {
		var expected = "Invalid value";
		var result = Assertions.assertThrows(IllegalArgumentException.class, () -> numberAPI.convert("1d"));
		Assertions.assertEquals(expected, result.getMessage());
	}

}
