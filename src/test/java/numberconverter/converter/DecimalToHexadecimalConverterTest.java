package numberconverter.converter;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import numberconverter.converter.Converter.ConverterType;

class DecimalToHexadecimalConverterTest extends ConverterTest {

	@BeforeEach
	void init() {
		converter = Converter.getInstance(ConverterType.HEXADECIMAL);
	}

	@Test
	@Override
	@DisplayName("Should return Hexa[5] for Decimal[5]")
	void convertFiveTest() {
		executeTest(5, "5");
	}

	@Test
	@Override
	@DisplayName("Should return Hexa[A] for Decimal[10]")
	void convertTenTest() {
		executeTest(10, "A");
	}

	@Test
	@Override
	@DisplayName("Should return Hexa[64] for Decimal[100]")
	void convertOneHundredTest() {
		executeTest(100, "64");
	}

	@Test
	@Override
	@DisplayName("Should return Hexa[3E8] for Decimal[1.000]")
	void convertOneThousandTest() {
		executeTest(1_000, "3E8");
	}

}
