package numberconverter.converter;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import numberconverter.converter.Converter.ConverterType;

class DecimalToOctalConverterTest extends ConverterTest {

	@BeforeEach
	void init() {
		converter = Converter.getInstance(ConverterType.OCTAL);
	}

	@Test
	@Override
	@DisplayName("Should return Octal[5] for Decimal[5]")
	void convertFiveTest() {
		executeTest(5, "5");
	}

	@Test
	@Override
	@DisplayName("Should return Octal[12] for Decimal[10]")
	void convertTenTest() {
		executeTest(10, "12");
	}

	@Test
	@Override
	@DisplayName("Should return Octal[144] for Decimal[100]")
	void convertOneHundredTest() {
		executeTest(100, "144");
	}

	@Test
	@Override
	@DisplayName("Should return Octal[1750] for Decimal[1.000]")
	void convertOneThousandTest() {
		executeTest(1_000, "1750");
	}

}
