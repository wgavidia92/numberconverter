package numberconverter.converter;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import numberconverter.converter.Converter.ConverterType;

class DecimalToBinaryConverterTest extends ConverterTest {

	@BeforeEach
	void init() {
		converter = Converter.getInstance(ConverterType.BINARY);
	}

	@Test
	@Override
	@DisplayName("Should return Binary[101] for Decimal[5]")
	void convertFiveTest() {
		executeTest(5, "101");
	}

	@Test
	@Override
	@DisplayName("Should return Binary[1010] for Decimal[10]")
	void convertTenTest() {
		executeTest(10, "1010");
	}

	@Test
	@Override
	@DisplayName("Should return Binary[1100100] for Decimal[100]")
	void convertOneHundredTest() {
		executeTest(100, "1100100");
	}

	@Test
	@Override
	@DisplayName("Should return Binary[1111101000] for Decimal[1.000]")
	void convertOneThousandTest() {
		executeTest(1_000, "1111101000");
	}

}
