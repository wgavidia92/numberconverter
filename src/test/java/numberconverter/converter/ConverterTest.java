package numberconverter.converter;

import org.junit.jupiter.api.Assertions;

abstract class ConverterTest {

	protected Converter converter;

	protected void executeTest(long value, String expected) {
		var result = converter.convert(value);
		Assertions.assertEquals(expected, result);
	}

	abstract void convertFiveTest();

	abstract void convertTenTest();

	abstract void convertOneHundredTest();

	abstract void convertOneThousandTest();

}
